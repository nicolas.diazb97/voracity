﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vida : MonoBehaviour
{
    public int bloodToAdd;
    bool once=true;
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 4, 0);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Gatto>() && once)
        {
            other.GetComponent<Gatto>().AddLife(bloodToAdd);
            once = false;
            Destroy(this.gameObject);
        }

    }
}
