﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineChangeColor : MonoBehaviour
{
    public Material openedColor;
    MeshRenderer skinMesh;

    // Start is called before the first frame update
    void Start()
    {
        skinMesh = GetComponent<MeshRenderer>();
    }

    public void ChangeColor()
    {
        Material[] mats = new Material[] {
        openedColor
        };
        skinMesh.GetComponent<MeshRenderer>().materials = mats;
    }
}
