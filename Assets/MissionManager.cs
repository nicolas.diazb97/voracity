﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
//using Invector.CharacterController;

public class MissionManager : Singleton<MissionManager>
{
    public List<Mission> pendingMissions = new List<Mission>();
    public List<Mission> missions = new List<Mission>();
    public List<string> currMissionParagraphs = new List<string>();
    public GameObject popUp;
    TextTyper typer;
    Text popUpText;
    Animator popUpAnim;
    int currParagraphIterator;
    int currParagraph = 0;
    public Gatto gatto;
    private void Start()
    {
        typer = GetComponent<TextTyper>();
        popUpText = popUp.GetComponentInChildren<Text>();
        popUpAnim = popUp.GetComponent<Animator>();
        missions = GetComponentsInChildren<Mission>().ToList();
    }
    public void ShowNextMission()
    {
        currParagraphIterator = GetCurrMission().description.Count;
        Debug.Log(GetCurrMission().description.Count);
        popUpText.text = "";
        SetCurrParagraph();
        popUpAnim.Play("in");
    }
    public void ComponentsGatto(bool activated)
    {
        //gatto.GetComponent<AttackManager>().enabled = activated;
        //gatto.GetComponent<vThirdPersonController>().enabled = activated;
        //gatto.GetComponent<vThirdPersonController>().isSprinting = false;
        //gatto.GetComponent<vThirdPersonInput>().enabled = activated;
        //gatto.GetComponent<InventoryController>().enabled = activated;
        //gatto.GetComponent<Gatto>().enabled = activated;
    }
    public void SetCurrParagraph()
    {
        //gatto.GetComponent<Animator>().SetBool("mission", true);
        //gatto.GetComponent<Animator>().Play("phone");
        //gatto.GetComponent<Rigidbody>().isKinematic = true;
        //gatto.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        ComponentsGatto(false);
        popUpText.text = "";
        if (currParagraph >= GetCurrMission().description.Count)
        {
            GetCurrMission().state = true;
            currParagraph = 0;
            popUpAnim.Play("out");
            gatto.GetComponent<Rigidbody>().isKinematic = false;
            ComponentsGatto(true);
            gatto.GetComponent<Animator>().SetBool("mission", false);
        }
        else
        {
            typer.textComp = popUpText;
            typer.ShowText(GetCurrMission().description.ElementAt(currParagraph));
            //popUpText.text = GetCurrMission().description.ElementAt(currParagraph);
            currParagraph++;

        }
    }
    public Mission GetCurrMission()
    {
        pendingMissions = GetComponentsInChildren<Mission>().Where(m => m.state != true).OrderBy(m => m.turnNumber).ToList();
        return pendingMissions.ElementAt(0);
    }
}
