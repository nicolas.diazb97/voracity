﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public class TroopController : MonoBehaviour
{
   
    ChandaController chandaController;
    TopoController topoController;
    RataController rataController;
    MiniBossController miniBossController;
    RatotaController ratotaController;
    public bool enemiesClear = false;
    Text chandaCount, topoCount, rataCount;

   // EnemiesController<RataController> rataController;
    private void Start()    {
        chandaCount = FindObjectOfType<ChandaText>().GetComponent<Text>();
        topoCount = FindObjectOfType<TopoText>().GetComponent<Text>();
        rataCount = FindObjectOfType<RataText>().GetComponent<Text>();
        chandaController = GetComponentInChildren<ChandaController>();
        topoController = GetComponentInChildren<TopoController>();
        miniBossController = GetComponentInChildren<MiniBossController>();
        rataController = GetComponentInChildren<RataController>();
        ratotaController = GetComponentInChildren<RatotaController>();
        chandaController.onFirstGattoSightController.AddListener(() =>
        {       
            chandaController.FirstGattoSight();
            topoController.FirstGattoSight();
            rataController.FirstGattoSight();
            miniBossController.FirstGattoSight();
        });
        miniBossController.onFirstGattoSightController.AddListener(() =>
        {
            miniBossController.FirstGattoSight();
            chandaController.FirstGattoSight();
            topoController.FirstGattoSight();
            rataController.FirstGattoSight();
        });
        topoController.onFirstGattoSightController.AddListener(() =>
        {
            miniBossController.FirstGattoSight();
            chandaController.FirstGattoSight();
            topoController.FirstGattoSight();
            rataController.FirstGattoSight();            
        });
         rataController.onFirstGattoSightController.AddListener(() =>
        {
            miniBossController.FirstGattoSight();
            chandaController.FirstGattoSight();
            topoController.FirstGattoSight();
            rataController.FirstGattoSight();            
        });

    }
    private void Update()
    {
        chandaCount.text = GetComponentsInChildren<Chanda>().ToList().Count().ToString();
        rataCount.text = GetComponentsInChildren<Rata>().ToList().Count().ToString();
        topoCount.text = GetComponentsInChildren<Topo>().ToList().Count().ToString();
        if (GetComponentInChildren<Enemy>())
        {

        }
        else
        {
            enemiesClear = true;
        }
    }
}
