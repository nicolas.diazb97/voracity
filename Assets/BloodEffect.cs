﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class BloodEffect : MonoBehaviour
{
    PostProcessVolume m_Volume;
    Vignette m_Vignette;
    float initVariable = 0;

    private void OnEnable()
    {
        initVariable = 0;
    }
    void Start()
    {
        m_Vignette = ScriptableObject.CreateInstance<Vignette>();
        m_Vignette.enabled.Override(true);
        m_Vignette.intensity.Override(1f);

        m_Volume = PostProcessManager.instance.QuickVolume(gameObject.layer, 100f, m_Vignette);
    }

    void Update()
    {
        initVariable += Time.deltaTime * 2;
        m_Vignette.intensity.value = Mathf.Abs(Mathf.Sin(initVariable));
    }

    void OnDisable()
    {
        m_Vignette.intensity.value = 0;
        initVariable = 0;
    }
}