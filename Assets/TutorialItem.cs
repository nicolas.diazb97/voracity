﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialItem : MonoBehaviour
{
    public KeyCode currKey;
    // Start is called before the first frame update
    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<Gatto>())
        {
            if (Input.GetKeyDown(currKey))
            {
                Destroy(this.gameObject);
            }
        }
    }
}
