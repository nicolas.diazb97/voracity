﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class RememberInventory : MonoBehaviour
{
    public List<Video> videos;
    public List<InventoryImage> covers;
    void Start()
    {
        videos = FindObjectsOfType<Video>().ToList();
        covers = GetComponentsInChildren<InventoryImage>(true).ToList();
        covers.ForEach(c => c.Start());
    }

    // Update is called once per frame
    void Update()
    {
        ListOfVideos();
    }

    public void ListOfVideos()
    {
        foreach (var v in videos)
        {
            if (v.check)
            {
                Debug.Log("Aquí se muestra la imagen del video :D");
                covers.ForEach(c => {
                    if (c.id == v.id)
                    {
                        c.gameObject.SetActive(true);
                        c.saveCover.sprite = v.cover;
                    }
                });
            }
        }
    }
}
