﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryImage : MonoBehaviour
{
    [HideInInspector]
    public Image saveCover;
    public int id;
    // Start is called before the first frame update
    public void Start()
    {
        saveCover = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
