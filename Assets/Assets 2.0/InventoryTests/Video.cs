﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class Video : MonoBehaviour
{
    //[HideInInspector]
    public int id;

    public VideoClip remember;
    public VideoPlayer vp;
    public Sprite cover;
    public bool check = false;
    // Start is called before the first frame update
    public void Play()
    {
        vp.clip = remember;
        vp.Play();
    }
    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("Go to remember....");
        if (!check)
        {
            Debug.Log("Playing remember....");
            Play();
        }
        check = true;
    }
}
