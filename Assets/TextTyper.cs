﻿/* *********************************************************
FileName: TextTyper.cs
Authors: Fabian Mendez <ofmendez@gmail.com>, 
   
Create Date: 14.11.2017
Modify Date: 27.11.2017 
********************************************************* */
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
 
public class TextTyper : MonoBehaviour {
 
     public float letterPause = 0.2f;
     public float waitTime = 3;
     public UnityEvent onEndText;
    bool OnMission;
     string message;
    public Text textComp;

    public void ShowText (string m)
    {
        OnMission = true;
        StopCoroutine(TypeText());
        message = m;
        StartCoroutine(TypeText ());
     }
 
     IEnumerator TypeText () {
         foreach (char letter in message.ToCharArray()) {
            if (OnMission == false)
            {
                break;
            }
             textComp.text += letter;
             yield return 0;
             yield return new WaitForSeconds (letterPause);
         }
         yield return new WaitForSeconds (waitTime);
         onEndText.Invoke();
     }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && OnMission)
        {
            textComp.text = message;
            OnMission = false;
        }
    }
}