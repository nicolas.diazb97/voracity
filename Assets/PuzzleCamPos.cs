﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleCamPos : MonoBehaviour
{
    public GameObject principalCamera;
    bool onPuzzle;
    bool onPuzzleOff;
    public bool ready;
    public PuzzleDoor door;
    GameObject gattoGeometry;
    public GameObject uinteractuar;
    bool adentro;
    bool cinematicPuzzle;

    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        uinteractuar.SetActive(true);
    }
    public void ClosePopUp()
    {
        uinteractuar.SetActive(false);
    }
    private void OnTriggerStay(Collider other)
    {

        if (Input.GetKeyDown(KeyCode.E))
        {

            if (other.GetComponentInChildren<GattoGeometry>())
            {

                gattoGeometry = other.GetComponentInChildren<GattoGeometry>().gameObject;

            }


            if (other.GetComponent<Gatto>() && ready)
            {


                if (!onPuzzle)
                {
                    onPuzzle = true;
                    uinteractuar.SetActive(true);
                    //uinteractuar.SetActive(false);

                }

            }

        }
    }
    private void OnTriggerExit(Collider other)
    {

        onPuzzle = false;
        uinteractuar.SetActive(false);

    }
    private void Update()
    {
        if (onPuzzle && !cinematicPuzzle)
        {
            //Debug.Log("dentro");
            OnPuzzle();
        }
        if (!onPuzzle && !cinematicPuzzle)
        {
            //Debug.Log("fuera");
            OffPuzzle();
        }
        if (cinematicPuzzle)
        {
            OffPuzzle();
        }
    }
    public void OnPuzzleEnded()
    {
        cinematicPuzzle = true;
    }
    IEnumerator WaitToCinematic()
    {
        yield return new WaitForSecondsRealtime(2f);
        CinematicManager.main.PlayVideo(2);
        cinematicPuzzle = false;
    }
    public void OnPuzzle()
    {
        uinteractuar.SetActive(false);
        Transform camPuzzle = FindObjectOfType<PuzzleCam>().transform;
        camPuzzle.GetComponent<Camera>().enabled = true;
        principalCamera.GetComponent<vThirdPersonCamera>().enabled = false;
        camPuzzle.transform.SetParent(transform);
        camPuzzle.transform.position = Vector3.Lerp(camPuzzle.transform.position, transform.position, 0.2f);
        camPuzzle.transform.rotation = Quaternion.Slerp(camPuzzle.transform.rotation, transform.rotation, 0.2f);
        gattoGeometry.SetActive(false);
    }
    public void OffPuzzle()
    {
        //uinteractuar.SetActive(false);
        if (gattoGeometry)
        {
            gattoGeometry.SetActive(true);
        }
        principalCamera.GetComponent<vThirdPersonCamera>().enabled = true;
        Transform camPuzzle = FindObjectOfType<PuzzleCam>().transform;
        camPuzzle.transform.SetParent(principalCamera.transform);
        camPuzzle.transform.position = Vector3.Lerp(camPuzzle.transform.position, principalCamera.transform.position, 0.2f);
        camPuzzle.transform.rotation = Quaternion.Slerp(camPuzzle.transform.rotation, principalCamera.transform.rotation, 0.2f);
        if (Vector3.Distance(camPuzzle.transform.position, principalCamera.transform.position) <= 1)
            camPuzzle.GetComponent<Camera>().enabled = false;
    }
    public void BeReady()
    {
        ready = true;
        door.locked = false;
    }
}
