﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateMission : MonoBehaviour
{
    public bool activated;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Gatto>()&&!activated)
        {
            MissionManager.main.ShowNextMission();
            activated = true;
        }
    }
}
