﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerZone : MonoBehaviour
{
    public TroopController currTroopOnAttack;
    public TroopController nextTroop;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("alguien toca");
        if (other.GetComponent<Gatto>())
        {
            Debug.Log("enemies clear");
            InteractableDoors.main.PlayAnimation(GetComponent<Animator>());
            currTroopOnAttack.gameObject.SetActive(false);
            nextTroop.gameObject.SetActive(true);
        }
    }
}
