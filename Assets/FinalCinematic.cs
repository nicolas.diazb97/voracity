﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalCinematic : MonoBehaviour
{
    public int cinematic;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Gatto>())
        {
            CinematicManager.main.PlayVideo(cinematic);
        }
    }
}
