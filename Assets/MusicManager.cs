﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : Singleton<MusicManager>
{
    public AudioSource source;

    private void Start()
    {
        source = GetComponent<AudioSource>();
    }
    public void PlayTrack(AudioClip trackToPlay)
    {
        source.PlayOneShot(trackToPlay);
    }
    public void PlayFinishedTrack(AudioClip trackToPlay)
    {
        source.clip = trackToPlay;
        source.Play();
    }
}
