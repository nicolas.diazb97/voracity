﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.VFX;

public class RatotaShoot : MonoBehaviour
{
    float initVariable;
    Gatto gatto;
    bool onMoving;
    public int shootPower = 20;
    public GameObject explosion;
    private void OnEnable()
    {
        gatto = FindObjectOfType<Gatto>();
        initVariable = 0;
    }
    private void Start()
    {        
        transform.LookAt(gatto.transform);
    }
    void Update()
    {
        transform.position += transform.TransformDirection(Vector3.forward * Time.deltaTime * 30f);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Gatto>())
        {
            ExplosionEffect();
            Destroy(this.gameObject);
            gatto.GetDamaged(shootPower, 0.5f);
        }
        if (other.tag == "WallToHide")
        {
            ExplosionEffect();
            Destroy(this.gameObject);
        }
    }
    public void ExplosionEffect()
    {
        GameObject TempExplotion;
        TempExplotion = Instantiate(explosion, transform.position + (transform.forward * 0.1f), transform.rotation);
        Destroy(TempExplotion, 8F);
    }
}
