﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatObstacle : MonoBehaviour
{
    Transform rat;
    void Start()
    {
        rat = transform.GetChild(0);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = rat.position;
        transform.rotation = rat.rotation;
    }
}
