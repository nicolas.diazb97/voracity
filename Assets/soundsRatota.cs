﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soundsRatota : MonoBehaviour
{
    public AudioClip jump;
    AudioSource audioSource;
    public AudioClip melee;
    public AudioClip melee2;
    public AudioClip impactFloor;
    public AudioClip death;
    public AudioClip electrocuted;
    public AudioClip blast;
    public AudioClip weaponTransform;

    public void Electrocuted()
    {
        audioSource.PlayOneShot(electrocuted);

    }
    public void TransformWeapon()
    {
        audioSource.PlayOneShot(weaponTransform);

    }
    public void JumpSound()
    {
        audioSource.PlayOneShot(jump);

    }
    public void MeleeSound()
    {
        audioSource.PlayOneShot(melee);
    }
    public void MeleeSound2()
    {
        audioSource.PlayOneShot(melee2);
    }
    public void ImpactFloorSound()
    {
        audioSource.PlayOneShot(impactFloor);
    }
    public void DeathSound()
    {
        audioSource.PlayOneShot(death);
    }
    public void DBlastSound()
    {
        audioSource.PlayOneShot(blast);
    }

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {

    }
}
