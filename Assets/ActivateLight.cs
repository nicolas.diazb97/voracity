﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ActivateLight : MonoBehaviour
{
    public List<GameObject> lightsToActivate = new List<GameObject>();
    public List<GameObject> lightsToDeactivate = new List<GameObject>();
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Gatto>())
        {
            lightsToActivate.ForEach(l =>
            {
                l.SetActive(true);
            });
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Gatto>())
        {
            lightsToDeactivate.ForEach(l =>
            {
                l.SetActive(false);
            });
        }
    }
}
