﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlashState : MonoBehaviour
{
    public GameObject clawSlash;
    public Transform initialContainer;
    public Transform container;

    public void TurnOn()
    {
        clawSlash.SetActive(true);
        clawSlash.transform.SetParent(container, false);
        //clawSlash.transform.position = new Vector3(21f, 3.699928f, 0f);
    }

    public void GoBackToGameObject()
    {
        //clawSlash.transform.SetParent(initialContainer, false);

    }

    public void TurnOff()
    {
        clawSlash.SetActive(false);
        clawSlash.transform.SetParent(initialContainer, false);
    }
}
