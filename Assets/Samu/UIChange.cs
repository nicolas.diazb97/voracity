﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIChange : MonoBehaviour
{
    public GameObject shotgun;
    public GameObject pistol;
    Gatto gattou;

    // Update is called once per frame

    private void Start()
    {
        gattou = FindObjectOfType<Gatto>();
        gattou.onTryEquipWeapon.AddListener((IAttack currAttack) =>
        {
            TryToChangeGunIcon(currAttack);
        });
    }
    void TryToChangeGunIcon(IAttack currAttack)
    {
        if(currAttack.name == "gun  verde")
        {
            pistol.SetActive(false);
            shotgun.SetActive(true);
        }
        if (currAttack.name == "pistol  Gatto")
        {
            pistol.SetActive(true);
            shotgun.SetActive(false);
        }
    }
}

