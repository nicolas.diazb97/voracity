﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopMusic : MonoBehaviour
{
    public void VolumeDownMusic()
    {
        StartCoroutine(LowVolumeMusic());
    }

    IEnumerator LowVolumeMusic()
    {
        MusicManager.main.source.volume -= 0.2f;
        yield return new WaitForSecondsRealtime(0.1f);
        StartCoroutine(LowVolumeMusic());
    }
}
