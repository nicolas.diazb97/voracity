﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{

    public List<string> names;
    public GameObject image;
    // Update is called once per frame
    public void ChangeTheScene()
    {
        image.SetActive(true);
    }
    public void LoadScenes()
    {
        SceneManager.LoadScene("Escena-Marce");
        //StartCoroutine(LoadScenesAsync(names));
        SceneManager.LoadScene("Logic+EffectsNico", LoadSceneMode.Additive);
    }
    private IEnumerator LoadScenesAsync(List<string> namesOfScenesToLoad)
    {
        Scene originalScene = SceneManager.GetActiveScene();

        List<AsyncOperation> sceneLoads = new List<AsyncOperation>();
        for (int i = 0; i < namesOfScenesToLoad.Count; ++i)
        {
            AsyncOperation sceneLoading = SceneManager.LoadSceneAsync(namesOfScenesToLoad[i], LoadSceneMode.Additive);
            sceneLoading.allowSceneActivation = false;

            sceneLoads.Add(sceneLoading);
            Debug.Log(sceneLoads[i].progress);
            while (sceneLoads[i].progress < 0.9f) { yield return null; }
        }
        // TODO: deactivate conflicting components from originalScene here
        // need to have at least one active scene before unloading the originalScene    
        for (int i = 0; i < sceneLoads.Count; ++i)
        {
            sceneLoads[i].allowSceneActivation = true;
            while (!sceneLoads[i].isDone) { yield return null; }
        }

        AsyncOperation sceneUnloading = SceneManager.UnloadSceneAsync(originalScene);
        while (!sceneUnloading.isDone) { yield return null; }
    }
    IEnumerator AsynchronousLoad()
    {
        yield return null;

        AsyncOperation ao = SceneManager.LoadSceneAsync("Logic+Effects", LoadSceneMode.Additive);
        ao.allowSceneActivation = false;

        float progress = Mathf.Clamp01(ao.progress / 0.9f);
        Debug.Log("Loading progress: " + (progress * 100) + "%");
        while (!ao.isDone)
        {
            // [0, 0.9] > [0, 1]

            // Loading completed
            if (ao.progress == 0.9f)
            {
                //ao.allowSceneActivation = true;
            }

            yield return null;
        }
    }
}
