﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOffCards : MonoBehaviour
{
    public GameObject cardsGameobject;

    // Update is called once per frame
    public void EndAnimation()
    {
        cardsGameobject.SetActive(false);
    }
}
