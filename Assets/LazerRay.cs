﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerRay : MonoBehaviour
{
    private float gunPower = 10;
    public float PosLazerZ;
    public GameObject impactLazer, laserline;
    LineRenderer lazer;
    // Start is called before the first frame update
    void Start()
    {
        lazer = GetComponentInChildren<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 100f))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            PosLazerZ = hit.distance;
//            Debug.Log(hit.transform.position);
            lazer.SetPosition(1, new Vector3(lazer.GetPosition(1).x, lazer.GetPosition(1).y, PosLazerZ));
            impactLazer.transform.position = hit.point + (hit.normal * 0.1f);
            if (hit.transform.GetComponent<Gatto>())
            {
                hit.transform.GetComponent<Gatto>().GetDamaged(gunPower, 0.5f);
            }
        }
    }
}
