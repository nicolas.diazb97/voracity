﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using System.Linq;

public class CinematicManager : Singleton<CinematicManager>
{
    bool once = true;
    public Image renderTexture;
    public Material[] materialVideos;
    public VideoClip[] videos;
    public VideoPlayer videoPlayer;
    public int[] durations;
    public GameObject aim, ui, blackBackground;
    public GameObject enemiesParent;
    List<EnemiesText> enemiesUI = new List<EnemiesText>();
    int actualVideo;
    bool onVideo, doneFirstCinematic, doneSecondCinematic;
    public MissionManager missionManager;
    int currCinematic;
    public GameObject cards;
    // Start is called before the first frame update
    void Start()
    {
        enemiesUI = enemiesParent.GetComponentsInChildren<EnemiesText>().ToList();
        PlayVideo(0);
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && onVideo)
        {
            if (PlayerPrefs.GetInt("visto" + actualVideo.ToString()) == 1)
            {
                cards.SetActive(true);
                Finish();
            }
        }
    }
    // Update is called once per frame
    public void PlayVideo(int cinematic)
    {
        currCinematic = cinematic;
        missionManager.ComponentsGatto(false);
        onVideo = true;
        blackBackground.SetActive(true);
        aim.SetActive(false);
        enemiesUI.ForEach(eu =>
        {
            if (eu.currObject.GetComponent<Image>())
            {
                eu.currObject.GetComponent<Image>().enabled = false;
            }
            if (eu.currObject.GetComponent<Text>())
            {
                eu.currObject.GetComponent<Text>().enabled = false;
            }
        });
        videoPlayer.gameObject.SetActive(true);
        videoPlayer.clip = videos[cinematic];
        renderTexture.gameObject.SetActive(true);
        renderTexture.material = materialVideos[cinematic];
        StartCoroutine(WaitToFinish(durations[cinematic]));
    }
    void Finish()
    {
        if (currCinematic == 0 && once)
        {
            cards.SetActive(true);
            once = false;
        }
        if (currCinematic == 1&&!doneFirstCinematic)
        {
            MainDoorColorChange.main.ChangeColor();
            MissionManager.main.ShowNextMission();
            StopCoroutine(WaitToFinish(currCinematic));
            doneFirstCinematic = true;
        }
        if (currCinematic == 3&&!doneSecondCinematic)
        {
            MissionManager.main.ShowNextMission();
            StopCoroutine(WaitToFinish(currCinematic));
            doneSecondCinematic = true;
        }
        if (currCinematic == 4)
        {
            ActivateFinalImage.main.Activate();
        }
        missionManager.ComponentsGatto(true);
        onVideo = false;
        aim.SetActive(true);
        ui.SetActive(true);
        enemiesUI.ForEach(eu =>
        {
            if (eu.currObject.GetComponent<Image>())
            {
                eu.currObject.GetComponent<Image>().enabled = true;
            }
            if (eu.currObject.GetComponent<Text>())
            {
                eu.currObject.GetComponent<Text>().enabled = true;
            }
        });
        blackBackground.SetActive(false);
        videoPlayer.gameObject.SetActive(false);
        renderTexture.gameObject.SetActive(false);
    }
    public IEnumerator WaitToFinish(int currVideo)
    {
        actualVideo = currVideo;
        yield return new WaitForSecondsRealtime(currVideo);
        PlayerPrefs.SetInt("visto" + currVideo.ToString(), 1);
        Finish();
    }

}

