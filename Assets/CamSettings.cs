﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamSettings : Singleton<CamSettings>
{
    public GameObject drugEffect;

    public void InstinctEffect(bool state)
    {
        drugEffect.SetActive(state);
    }
}
