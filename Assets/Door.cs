﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public TroopController currTroopOnAttack;
    public TroopController nextTroop;
    public AudioClip trackToPlay;
    public bool playTrack = true;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("alguien toca");
        if (other.GetComponent<Gatto>())
        {
            Debug.Log("toca gatto");
            if (currTroopOnAttack.enemiesClear)
            {
                GetComponent<BoxCollider>().enabled = false;
                Debug.Log("enemies clear");
                InteractableDoors.main.PlayAnimation(GetComponent<Animator>());
                currTroopOnAttack.gameObject.SetActive(false);
            }
        }
    }
    public void OpenDoor()
    {
        if (playTrack)
        {
            MusicManager.main.PlayTrack(trackToPlay);
        }
        nextTroop.gameObject.SetActive(true);
    }
}
