﻿using System.Collections;
using System.Collections.Generic;

public class AttackStats
{
    public float weight;
    public float damage;
    public int frequency;
    public float scope;


    public AttackStats(float ReturnedWeight, float ReturnedDamage, int RetunedFrequency, float ReturnedScope)
    {
        weight = ReturnedWeight;
        damage = ReturnedDamage;
        frequency = RetunedFrequency;
        scope = ReturnedScope;
    }


    public void RemoveStats()
    {
        weight = 0;
       damage = 0;
       frequency = 0;
        scope = 0;
        

        }
  
}
