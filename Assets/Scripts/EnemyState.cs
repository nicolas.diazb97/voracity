﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[System.Serializable]
public abstract class EnemyState :MonoBehaviour
{
    public abstract void Process(NavMeshAgent agent);
}
