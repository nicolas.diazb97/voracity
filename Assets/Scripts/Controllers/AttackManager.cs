﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AttackManager : MonoBehaviour
{

    GameObject playerHand;
    AttackContainer attackContainer;
    Garra garra;
    public IAttack currentAttack;
    Gatto gatto;
    Transform chest;
    Transform hand;
    Transform target;
    public Vector3 AimOffset;
    bool onAiming = false, OnReturningEnded = true, onSelecting = false;
    public Transform animTarget;
    Quaternion lastChest, FirstChest;
    BoxCollider handCollider;
    void Start()
    {
        FirstChest = GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest).rotation;
        lastChest = GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest).rotation;
        target = FindObjectOfType<AimTarget>().transform;
        attackContainer = FindObjectOfType<AttackContainer>();
        chest = GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest);
        hand = GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftHand);
        gatto = GetComponent<Gatto>();
        garra = GetComponentInChildren<Garra>();
        playerHand = GetComponentInChildren<Hand>().gameObject;
        gatto.OnAiming.AddListener((state, animator) =>
        {
            TryToAim(state, animator);
        });
        gatto.onTryEquipWeapon.AddListener((attack) =>
        {
            TryToEquip(attack);
        });
        gatto.onPerfomAttack.AddListener((animator) =>
        {
            PerfomAttack(animator);
        });
        gatto.onCollect.AddListener((transform) =>
        {
            FindClosestAttack(transform);
        });


    }


    void FindClosestAttack(Transform gattoTransform)
    {

        float closestDistance = 2;
        //var result = attacks.OrderByDescending(g => Vector3.Distance(g.GetGameObject().transform.position, gattoTransform.position));
        Attack[] attacks = attackContainer.GetComponentsInChildren<Attack>().OrderBy(g => g.DistanceTo(gattoTransform)).ToArray();
        if (attacks.Length > 0 && attacks[0].DistanceTo(gattoTransform) < closestDistance)
        {
            gatto.EquipWeapon(attacks[0]);
        }
    }
    void TryToEquip(IAttack attack)
    {
        if (gatto.isEquipped && currentAttack.GetGameObject() == attack.GetGameObject())
            return;
        if (gatto.isEquipped)
        {
            ReleaseAttack();
        }
        Equip(attack);
    }
    void TryToAim(bool state, Animator anim)
    {
        if (gatto.isEquipped)
        {
            currentAttack.TryToAim(state, anim);
        }
    }
    void Equip(IAttack attack)
    {
        currentAttack = attack;
        currentAttack.SetPhysics(false);
        currentAttack.GetGameObject().transform.localPosition = playerHand.transform.position;
        currentAttack.GetGameObject().transform.localRotation = playerHand.transform.rotation;
        currentAttack.GetGameObject().transform.SetParent(playerHand.transform);
        gatto.LoadStats(currentAttack);
        gatto.isEquipped = true;
        Debug.Log(currentAttack.name);

    }
    private void ReleaseAttack()
    {
        gatto.ReleaseStats(currentAttack);
        currentAttack.GetGameObject().transform.SetParent(attackContainer.transform);
        currentAttack.SetPhysics(true);
    }

    public void InitGarra(Item GarraAttack)
    {
        //garra.GetComponent<IAttack>().stats = GarraAttack.Stats;
        //Debug.Log(GarraAttack.Stats);
        //characterStats.AddStatBonus(GarraAttack.Stats);
    }
    void PerfomAttack(Animator anim)
    {
        if (gatto.isEquipped)
        {
            currentAttack.PerformAttack(anim);
            Debug.Log("attacking with " + currentAttack.name);
        }
    }
    public void TryToPuzzle(bool state)
    {
        gatto.GetComponent<Animator>().Play("ApuntarAbajo");
        onSelecting = state;
    }
    public void TryToIkAiming(bool state)
    {
        if (currentAttack is Escopeta)
        {
            AimOffset = new Vector3(0f, -60f, -62.9f);
        }
        else
        {
            AimOffset = ((gatto.GetComponent<Animator>().GetFloat("InputVertical")) > 0.5f) ? new Vector3(0f, -93.8f, -78f) : new Vector3(0f, -93.8f, -62.9f);
            //(0f, -93.8f, -78f);
            //(0f, -93.8f, -62.9f);
        }
        OnReturningEnded = false;
        onAiming = state;

    }
    private void LateUpdate()
    {
        if (onSelecting)
        {
            Vector3 mouse = Input.mousePosition;
            Ray castPoint = Camera.main.ScreenPointToRay(mouse);
            RaycastHit hit;
            if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))
            {
                Vector3 offset = new Vector3(hit.point.x, hit.point.y, gatto.transform.position.z * -1.2f);
                hand.transform.position = FindObjectOfType<CalcButton1>().transform.position;
            }
        }
        if (onAiming)
        {
            chest.LookAt(target.position);
            chest.rotation *= Quaternion.Euler(AimOffset);
            lastChest = chest.rotation; if (currentAttack is Escopeta)
            {
                gatto.GetComponent<Rigidbody>().isKinematic = true;
            }
        }
        else if (OnReturningEnded == false)
        {
            gatto.GetComponent<Rigidbody>().isKinematic = false;
            if (GetComponent<Animator>().GetBool("OnAiming") == false)
            {
                chest.rotation = Quaternion.Slerp(lastChest, GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest).rotation, 0.2f);
                lastChest = chest.rotation;
            }
            else
            {
                chest.rotation = Quaternion.Slerp(lastChest, GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest).rotation, 0.02f);
                lastChest = chest.rotation;
            }



        }
        //public void PerformMeleeAttack(Animator anim)
        //{
        //    garra.PerformAttack(anim);
        //}
    }
}
