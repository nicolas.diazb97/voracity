﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class EnemiesController<T> : MonoBehaviour
{
    protected List<Enemy> enemies = new List<Enemy>();
    protected List<HidePanel> hidePanels = new List<HidePanel>();
    Gatto gatto;
    public int enemiesToAttack;
    public UnityEvent onFirstGattoSightController;


    private void Start()
    {
        //Debug.Log("ESTA EN LA LISTA: " + gameObject.name);
        gatto = FindObjectOfType<Gatto>();
        hidePanels = FindObjectsOfType<HidePanel>().ToList();
        enemies = GetComponentsInChildren<T>().Cast<Enemy>().ToList();
        RaycastHit hit;

        enemies.ForEach(e =>
        {
            e.onFirstGattoSight.AddListener(() => onFirstGattoSightController.Invoke() );
        });

        enemies.ForEach(e =>
        {
            e.onDeadEnemy.AddListener(() =>
            {
                Debug.Log("murio alguien");
                AssignList();
            });
        });

        gatto.onPerfomAttack.AddListener((animator) =>
        {
            DetectShot();
        });
    }

    public void FirstGattoSight()
    {
        AssignList();
        enemies.ForEach(e =>
        {
            Debug.Log("yo lo vi: " + e.name);
            e.onFirstGattoSight.RemoveAllListeners();
        });
    }

    protected void GattoOnSight()
    {
        OrderList();
    }
    protected void AssignList()
    {
        enemies = GetComponentsInChildren<T>().Cast<Enemy>().ToList();
        OrderList();
    }
    protected void OrderList()
    {
        enemies.Clear();
        enemies = GetComponentsInChildren<T>().Cast<Enemy>().Where(e => !(e.CurrState() is EDead)).OrderBy(e => e.DistanceTo(gatto.transform)).ToList();
        //enemies = enemies.OrderBy(e => e.DistanceTo(gatto.transform)).ToList();
        ToSafePlace();
        enemies.Take(enemiesToAttack).ToList().ForEach(e =>
        {
            if (!(e.CurrState() is EAttack))
            {
                e.SetState(new EAttack());
            }
        });
        enemies.Skip(enemiesToAttack).ToList().ForEach(e =>
        {
            e.SetState(new EHide());
        });
    }

    private void ToSafePlace()
    {
        enemies.ForEach(e =>
        {
            if (e.CurrState() is EDead)
            {
            }
            else
            {
                List<HidePanel> tempHidePanels;
                tempHidePanels = hidePanels.Where(p => p.IsOcuppied == false).ToList();
                tempHidePanels = hidePanels.OrderBy(p => p.DistanceTo(e.transform)).ToList();
                e.nearestPanel = tempHidePanels[0].transform;
                tempHidePanels[0].IsOcuppied = true;
            }
        });
    }

    protected void DetectShot()
    {
        if (gatto.isEquipped)
        {
            enemies.ForEach(e =>
            {
                if (e.CurrState() is EPatrol)
                    e.SetState(new ESearch());
            });
        }
    }
}
