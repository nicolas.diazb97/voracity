﻿using System.Collections;
using UnityEngine;

public class InventoryController : MonoBehaviour
{
    public Gun gun;

    Gatto gatto;
    Animator anim;
    AttackManager attackManager;
    bool shoot, firstShot;
    int counter = 0;
    //    public ICollectable sword, garra;

    private void Start()
    {
        anim = GetComponent<Animator>();
        gatto = GetComponent<Gatto>();
        attackManager = GetComponent<AttackManager>();
    }

    private void Update()
    {
        //if (Input.GetAxis("Shoot") > 0)
        //{
        //    if (firstShot)
        //    {
        //        shoot = true;
        //        firstShot = false;
        //    }
        //}
        //if (Input.GetAxis("Shoot") <= 0)
        //{
        //    firstShot = true;
        //}
        //Debug.Log("shoot: " + Input.GetAxis("Shoot") + "aim: " + Input.GetAxis("Aim"));
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("esto pasa muchas veces?");
            attackManager.TryToIkAiming(true);
        }
        if (Input.GetMouseButton(0))
        {
            counter++;
            Debug.Log(counter);
            if(counter>5)
                attackManager.TryToIkAiming(false);
        }
        else
        {
            counter = 0;
        }

        if (Input.GetMouseButtonUp(0))
        {
            attackManager.TryToIkAiming(false);
            //attackManager.AimOffset = Vector3.Slerp(attackManager.AimOffset, new Vector3(0, 0, 0), 0.02f);
        }
        if (Input.GetMouseButtonDown(1))
        {
            //Debug.Log("si lo detecto");
            //Time.timeScale = 0.5F;
            attackManager.TryToIkAiming(true);
            gatto.TryToAim(true, anim);
        }
        if (Input.GetMouseButtonUp(1))
        {
            //Time.timeScale = 1;
            attackManager.TryToIkAiming(false);
            gatto.TryToAim(false, anim);
            //attackManager.AimOffset = Vector3.Slerp(attackManager.AimOffset, new Vector3(0, 0, 0), 0.02f);
        }
        if (Input.GetKeyDown(KeyCode.E) || Input.GetButtonDown("Collect"))
        {
            gatto.Collect();
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            // playerWeaponController.PerformMeleeAttack(anim);

        }
        if (Input.GetMouseButtonDown(0))
        {
            gatto.PerfomAttack(anim);
            shoot = false;
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            //StopInstict();
            attackManager.TryToPuzzle(true);
        }
        if (Input.GetKeyDown(KeyCode.R))
        {

        }
        if (Input.GetKeyUp(KeyCode.Q))
        {
            //Debug.Log("getkeyup");
            //gatto.OffHide();
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            //Time.timeScale = 0.5F;
            //CamSettings.main.InstinctEffect(true);
            //StartCoroutine(CoolDownInstict());
            //Debug.Log("getkeydown");
            //gatto.Hide();
        }
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
        {
            attackManager.TryToIkAiming(false);
        }
    }
    void StopInstict()
    {
        Time.timeScale = 1;
        CamSettings.main.InstinctEffect(false);
    }
    IEnumerator CoolDownInstict()
    {
        yield return new WaitForSecondsRealtime(10f);
        StopInstict();
    }
}
