﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager>
{
    public AudioClip[] GattoSteps;
    public AudioClip Rugido;
    public AudioSource Gatto;
    public AudioSource LeftFoot;
    public AudioSource RightFoot;
    public GameObject Cam;
    // Start is calledG before the first frame update
    void Start()
    {
        Cam.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PlaySound(string Foot)
    {
        if (Foot == "Right")
        {
            RightFoot.PlayOneShot(GattoSteps[Random.Range(0, GattoSteps.Length)]);
        }
        if (Foot == "Left")
        {
            LeftFoot.PlayOneShot(GattoSteps[Random.Range(0, GattoSteps.Length)]);
        }
        if (Foot == "Garra")
        {

            Gatto.PlayOneShot(Rugido);
        }
    }
    public void AudioPlay(AudioSource ASource, AudioClip Clip)
    {
        ASource.PlayOneShot(Clip);
    }
}
