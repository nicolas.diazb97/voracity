﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Escopeta : Attack
{
    public override AttackStats Stats { get; set; }
    public AudioClip clip;
    AudioSource aSource;
    bool shooting = false, bursting = false, aimZoom = false;
    float distanceToZoom = 4.66f, heightToZoom = 2.02f;
    GameObject sight;
    public GameObject bulletHole;
    public GameObject blood;
    public GameObject muzzle;
    bool OnAttack = false;
    Animator anim2;
    float gunPower = 460;

    void Start()
    {
        aSource = GetComponent<AudioSource>();
        Stats = new AttackStats(1.07f, 80, 2, 40);
        sight = GetComponentInChildren<Sight>().gameObject;
        attacking = true;
    }
    public override void PerformAttack(Animator anim)
    {
        if (attacking)
        {
            attacking = false;
            bursting = (shooting) ? true : false;
            shooting = true;
            OnAttack = true;//
            anim2 = anim;//
            anim.Play("Escopeta");
            //anim.SetBool("OnAiming", true);
            SoundManager.main.AudioPlay(aSource, clip);
            StartCoroutine(CheckOnBurst());
            GameObject tempMuzzle;
            tempMuzzle = Instantiate(muzzle, sight.transform.position, sight.transform.rotation);
            tempMuzzle.transform.SetParent(this.transform);
            AffectOther();
            StartCoroutine(ShootEnded());
        }
    }
    public IEnumerator ShootEnded()
    {
        yield return new WaitForSecondsRealtime(1f);
        attacking = true;
    }
    IEnumerator CheckOnBurst()
    {
        yield return new WaitForSecondsRealtime(0.3f);
        if (!bursting)
        {
            shooting = false;
            anim2.SetBool("OnAiming", false);
        }
        bursting = false;
    }
    private void AffectOther()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.TransformDirection(Vector3.forward), out hit, 100f))
        {
            if (hit.transform.GetComponent<Enemy>())
            {
                if (hit.transform.GetComponent<Enemy>().CurrState() is EDead) { }
                else
                {

                }
                hit.transform.GetComponent<Enemy>().GetDamaged(gunPower, 0.5f);
                if (hit.transform.GetComponent<Ratota>())
                {
                    hit.transform.GetComponent<Enemy>().SetState(new EAttack());
                }
            }
            if (hit.transform.GetComponent<Rigidbody>())
            {
                hit.transform.GetComponent<Rigidbody>().AddForce(-hit.normal * 460f);
            }
            else
            {
                GameObject TempBulletHole;
                TempBulletHole = Instantiate(bulletHole, hit.point + (hit.normal * 0.1f), Quaternion.Euler(hit.normal));
                TempBulletHole.transform.SetParent(hit.transform);
                Destroy(TempBulletHole, 1F);
                TempBulletHole.transform.rotation = Quaternion.LookRotation(hit.normal);
                if (hit.transform.GetComponent<Enemy>())
                {
                    Debug.Log("funcionando");
                    TempBulletHole.GetComponentInChildren<ImageHole>().gameObject.SetActive(false);
                }
            }
        }
    }
    private void Update()
    {

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.TransformDirection(Vector3.forward), out hit, 100f))
        {
            if (hit.transform.GetComponent<Enemy>())
            {
                hit.transform.GetComponent<Enemy>().GattoIsAiming = true;
            }
            Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
        }
    }



    public override float GetWeight()
    {
        return Stats.weight;
    }

    public override GameObject GetGameObject()
    {
        return gameObject;
    }

    public override void TryToAim(bool state, Animator anim)
    {
        anim.Play("Escopeta");
        anim.SetBool("OnAiming", state);
        //Debug.Log(state);
        Camera.main.GetComponent<vThirdPersonCamera>().defaultDistance = (state) ? distanceToZoom : 2.97f;
        Camera.main.GetComponent<vThirdPersonCamera>().height = (state) ? heightToZoom : 2.02f;
        aimZoom = true;
    }
}
