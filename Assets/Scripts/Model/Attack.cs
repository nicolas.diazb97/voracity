﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Attack : MonoBehaviour, IAttack
{
    public string AttackName;
    public bool attacking;
    public abstract AttackStats Stats { get ; set; }

    private void Start()
    {
        name = AttackName;
    }
    public float DistanceTo(Transform t)
    {
        return Vector3.Distance(gameObject.transform.position,t.position);
    }

    public void SetPhysics(bool state)
    {
        GetComponent<Rigidbody>().isKinematic = !state;
    }

    public abstract GameObject GetGameObject();

    public abstract float GetWeight();

    public abstract void PerformAttack(Animator anim);

    public abstract void TryToAim(bool state, Animator anim);

    public bool CheckAttacking()
    {
        return attacking;
    }
}
