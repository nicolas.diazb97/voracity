﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;

public class EPatrol : IEnemyState
{
    int destPoint = 0;
    NavMeshAgent agent = null;
    RoutePoint[] pointsPatrol = null;

    public void Init(NavMeshAgent agent)
    {
        agent.GetComponent<Enemy>().InitPatrol();
    }

    public void Process(NavMeshAgent _agent, Gatto gatto)
    {
        agent = _agent;
        pointsPatrol = agent.gameObject.GetComponent<Enemy>().routePatrol.gameObject.GetComponentsInChildren<RoutePoint>();
        if (!agent.pathPending && agent.remainingDistance < 0.5f)
        {
            GotoNextPointPatrol();
        }
    }

    public void StopMyCoroutine()
    {

    }

    void GotoNextPointPatrol()
    {
        if (pointsPatrol.Length == 0)
            return;
        agent.destination = pointsPatrol[destPoint].transform.position;
        destPoint = (destPoint + 1) % pointsPatrol.Length;
    }
}
public class ESearch : IEnemyState
{
    int destPoint = 0;
    NavMeshAgent agent = null;
    RoutePoint[] pointsSearch = null;

    public void Init(NavMeshAgent agent)
    {
        agent.GetComponent<Enemy>().InitSearch();
    }

    public void Process(NavMeshAgent _agent, Gatto gatto)
    {
        agent = _agent;
        pointsSearch = agent.gameObject.GetComponent<Enemy>().routeSearch.gameObject.GetComponentsInChildren<RoutePoint>();
        if (!agent.pathPending && agent.remainingDistance < 0.5f)
        {
            GotoNextPointSearch();
        }
    }


    void GotoNextPointSearch()
    {
        if (pointsSearch.Length == 0)
            return;
        agent.destination = pointsSearch[destPoint].transform.position;
        destPoint = (destPoint + 1) % pointsSearch.Length;
    }
}
public class EThreat : IEnemyState
{
    public void Init(NavMeshAgent agent)
    {
        throw new System.NotImplementedException();
    }

    public void Process(NavMeshAgent _agent, Gatto gatto)
    {
    }

}
public class EAttack : IEnemyState
{
    int destPoint = 0;
    NavMeshAgent agent = null;
    RoutePoint[] pointsSearch = null;
    Enemy currEnemy;
    Animator currAnimator;
    public void Init(NavMeshAgent agent)
    {
        currEnemy = agent.gameObject.GetComponent<Enemy>();
        currAnimator = agent.gameObject.GetComponent<Animator>();
        currAnimator.speed = 1.3f;
        currEnemy.chanceToShoot = 1f;
        currAnimator.SetBool("OnAttack", true);
        agent.speed = 2f;
        //currEnemy.StartCoroutine(Decide());
        Debug.Log("init ataque: " + currEnemy.name);
    }

    public void Process(NavMeshAgent _agent, Gatto gatto)
    {
        currEnemy.Attack(gatto);
    }
    bool Shoot(float probability)
    {
        return
        Random.Range(0.00f, 1.00f) < probability;
    }

}
public class EHide : IEnemyState
{
    int destPoint = 0;
    NavMeshAgent agent = null;
    RoutePoint[] pointsSearch = null;
    Enemy currEnemy;
    Animator currAnimator;
    public void Init(NavMeshAgent agent)
    {
        currEnemy = agent.gameObject.GetComponent<Enemy>();
        currAnimator = agent.gameObject.GetComponent<Animator>();
        currEnemy.chanceToShoot = 0.12f;
        currAnimator.SetBool("OnAttack", true);
        currAnimator.speed = 1.8f;
        agent.speed = 2.5f;
        currAnimator.SetBool("OnAttack", true);
        agent.GetComponentInChildren<GraphicState>().gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.green);
        Debug.Log("init hide: " + currEnemy.name);
    }

    public void Process(NavMeshAgent _agent, Gatto gatto)
    {
        currEnemy.Hide(gatto);
    }
}
public class EDead : IEnemyState
{
    public void Init(NavMeshAgent agent)
    {
        agent.GetComponent<Enemy>().DeadEffect();
    }

    public void Process(NavMeshAgent _agent, Gatto gatto)
    {
    }

}





