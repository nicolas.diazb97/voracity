﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;
using DigitalRuby.LightningBolt;
using UnityEngine.UI;

public class Ratota : Enemy
{
    public AudioClip[] Sonidos;
    public GameObject uiLifeRatota;
    public Transform eyes, sight;
    public AudioClip clip;
    public GameObject bloodParticle;
    public GameObject visualEffect;
    float gunPower = 560f;
    public Transform hand;
    public GameObject bulletPool;
    public GameObject rayRatota;
    public GameObject center;
    NavMeshAgent agent;
    Animator anim;
    bool firstRay = true;
    public GameObject ray;
    public GameObject lazerRay;
    float t = 0;
    float rayDuration = 10;
    List<LineRenderer> rays = new List<LineRenderer>();
    bool readyToShoot = true;
    private Transform chest;
    public Vector3 aimOffset = new Vector3(0f, -93.8f, -78f);
    public Material electricMat, normalMap;
    GameObject skinMesh;
    bool onJump = false;
    float energy;
    public Slider sliderEnergy;
    public Slider sliderHealth;
    public Image fill;
    bool firstMeleeJump = true, jumpAttack = false;
    public GameObject energyBullet;
    Vector3 lastGattoPos;
    private void Start()
    {
        base.Start();
        energy = 0;
        skinMesh = FindObjectOfType<RatotaSkin>().gameObject;
        chest = GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest);
        blood = 15000f;
        eyes = GetComponentInChildren<Eyes>().transform;
        sight = GetComponentInChildren<Sight>().transform;
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        rays = ray.GetComponentsInChildren<LineRenderer>().ToList();
    }
    void Update()
    {
        base.Update();
        if (energy <= 0)
        {
            readyToShoot = true;
        }
        sliderEnergy.value = energy / 100f;
        sliderHealth.value = (blood * 100 / 15000) / 100;
        //sliderEnergy.value = energy.Remap(0, rayDuration, 0.00f, 1.00f);
        fill.color = Color.Lerp(Color.green, Color.red, t);
        RaycastHit hit;
        if (eyes)
        {
            if (Physics.Raycast(eyes.transform.position, eyes.TransformDirection(Vector3.forward), out hit, 200f))
            {
                if (hit.transform.GetComponent<Gatto>())
                {
                    this.SetState(new EAttack());
                }
                Debug.DrawRay(eyes.transform.position, eyes.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            }
        }
    }
    IEnumerator WaitToOverCharge()
    {
        yield return new WaitForSecondsRealtime(rayDuration);
        anim.Play("DamagedRay");
        ray.SetActive(false);
        yield return new WaitForSecondsRealtime(4f);
        anim.SetBool("RayEnded", true);
        yield return new WaitForSecondsRealtime(8f);
        ResetRay();
    }
    public void PowerRayOff()
    {
        readyToShoot = false;
    }
    public override void GetDamaged(float gunPower, float BodyPartMultiplier)
    {
        base.GetDamaged(gunPower, BodyPartMultiplier);
        anim.SetBool("Injured", true);
        StartCoroutine(WaitToFinishPain());
    }
    public void DamagedByRay()
    {
        GetDamaged(30, 0.5f);
    }
    IEnumerator WaitToFinishPain()
    {
        yield return new WaitForSecondsRealtime(0.2f);
        anim.SetBool("Injured", false);
    }
    private void ResetRay()
    {
        rays.ForEach(r =>
        {
            r.GetComponent<LightningBoltScript>().ChaosFactor = 0.08f;
        });
        t = 0;
        firstRay = true;
        energy = 100;
        anim.SetBool("ReadyToShoot", false);
    }
    public void Jump()
    {
        onJump = true;
    }
    public override void Attack(Gatto gatto)
    {
        if (readyToShoot)
        {
            float dist = Vector3.Distance(agent.transform.position, center.transform.position);
            if (dist > 10)
            {
                anim.Play("JumpAttack");
                agent.gameObject.transform.LookAt(center.transform);
            }
            if (dist < 10 && dist > 1)
            {
                if (!onJump)
                {
                    Debug.Log("pasa");
                    agent.isStopped = false;
                    agent.speed = 2.5f;
                    agent.gameObject.GetComponent<Animator>().speed = 1.8f;
                    agent.SetDestination(center.transform.position);
                }
            }
            if (onJump)
            {
                transform.position = Vector3.Lerp(transform.position, center.transform.position, 0.2f);
                agent.isStopped = true;
            }
            if (dist <= 1f)
            {
                onJump = false;
                anim.SetBool("Staging", true);
                agent.isStopped = true;
                rays.ForEach(r =>
                {
                    r.endColor = Color.Lerp(Color.green, Color.red, t);
                    if (t < 1)
                    { // while t below the end limit...
                      // increment it at the desired rate every update:
                        t += Time.deltaTime / (rayDuration * 3);
                    }
                    r.GetComponent<LightningBoltScript>().ChaosFactor += 0.001f;
                });
                agent.gameObject.transform.LookAt(gatto.transform);
                anim.SetBool("ReadyToShoot", true);
                if (firstRay)
                {
                    ray.SetActive(true);
                    firstRay = false;
                    StartCoroutine(WaitToOverCharge());
                }
                energy += Time.deltaTime * 100f / rayDuration;
            }
        }
        else
        {
            anim.SetBool("Staging", false);
            energy -= Time.deltaTime * 100f / rayDuration / 4;
            agent.isStopped = false;
            agent.SetDestination(gatto.transform.position);
            float dist = Vector3.Distance(agent.transform.position, gatto.transform.position);
            if (dist > 12)
            {
                chanceToShoot = 0.2f;
                if (anim.GetBool("DecideToShoot"))
                {
                    agent.isStopped = true;
                    anim.Play("SingleShoot");
                }
                else
                {
                    if (firstMeleeJump)
                    {
                        lastGattoPos = gatto.transform.position;
                        jumpAttack = true;
                        anim.SetBool("MeleeJump", true);
                        anim.Play("JumpAttack");
                        firstMeleeJump = false;
                        StartCoroutine(WaitToJump());
                    }
                }
                agent.gameObject.transform.LookAt(gatto.transform);
            }
            if (dist < 6)
            {
                onJump = false;
                anim.SetBool("Melee", true);
                agent.isStopped = true;
                agent.gameObject.transform.LookAt(gatto.transform);
            }
            else
            {
                anim.SetBool("Melee", false);
            }
            if (onJump)
            {
                Debug.Log("ejecutandose");
                Vector3 heading = gatto.transform.position - transform.position;
                float distance = heading.magnitude;
                Vector3 direction = heading / -distance;
                transform.position = Vector3.Lerp(transform.position, gatto.transform.position+(direction*0.5f), 0.2f);
                agent.isStopped = true;
            }
        }
    }
    IEnumerator WaitToJump()
    {
        yield return new WaitForSecondsRealtime(1f);
        firstMeleeJump = true;
        yield return new WaitForSecondsRealtime(1f);
        jumpAttack = false;
    }
    public void ActivateLazer()
    {
        lazerRay.SetActive(true);
    }
    public void EnableElectricShoot()
    {
        Material[] mats = new Material[] {
        skinMesh.GetComponent<SkinnedMeshRenderer>().materials[0],
        skinMesh.GetComponent<SkinnedMeshRenderer>().materials[1],
        electricMat
        };
        skinMesh.GetComponent<SkinnedMeshRenderer>().materials = mats;
        DamagedByRay();

    }
    public void SingleShoot()
    {
        GameObject TempEnergyBullet;
        TempEnergyBullet = Instantiate(energyBullet,lazerRay.transform.position,lazerRay.transform.rotation);
    }
    public void DisableElectricShoot()
    {
        Material[] mats = new Material[] {
        skinMesh.GetComponent<SkinnedMeshRenderer>().materials[0],
        skinMesh.GetComponent<SkinnedMeshRenderer>().materials[1],
        normalMap
        };
        skinMesh.GetComponent<SkinnedMeshRenderer>().materials = mats;

    }
    public void DeActivateLazer()
    {
        lazerRay.SetActive(false);
    }
    public void DamagedEffect()
    {
        bloodParticle.SetActive(true);
    }
    public void DamagedEffectOff()
    {
        bloodParticle.SetActive(false);
    }
    public override void Hide(Gatto gatto)
    {

    }
    public override void Threat(Gatto gatto)
    {

    }
    public override void DeadEffect()
    {
        uiLifeRatota.SetActive(false);
        FinalDoor.main.final();
        this.gameObject.GetComponent<Animator>().speed = 1f;
        agent.speed = 0.9f;
        anim.Play("Death");
        Collider[] colliders;
        colliders = GetComponents<Collider>();
        for (int i = 0; i < colliders.Length; i++)
        {

            MonoBehaviour.Destroy(colliders[i]);
        }
        MonoBehaviour.Destroy(this.gameObject.GetComponent<NavMeshAgent>());
        MonoBehaviour.Destroy(this.gameObject.GetComponent<Enemy>());
        MonoBehaviour.Destroy(this.gameObject, 120f);
    }
    public override void InitSearch()
    {
        int randomSearch = UnityEngine.Random.Range(0, 10);
        Debug.Log(randomSearch);
        if (randomSearch < 5)
        {
            agent.gameObject.GetComponent<Animator>().SetFloat("AgentVelocity", 1.8f);
        }
        else
        {
            agent.speed = 2.5f;
            agent.gameObject.GetComponent<Animator>().speed = 1.8f;
        }

    }
    public override void InitPatrol()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.gameObject.GetComponent<Animator>().speed = 1f;
        agent.speed = 2f;
        agent.gameObject.GetComponent<Animator>().SetFloat("AgentVelocity", 1);

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Gatto>())
        {
            if (anim.GetBool("Melee"))
            {
                other.GetComponent<Gatto>().GetDamaged(80, 0.5F);
            }
            if (jumpAttack)
            {
                other.GetComponent<Gatto>().GetDamaged(30, 0.5F);
            }
        }
    }
}
