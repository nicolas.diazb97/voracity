﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Rata : Enemy
{
    public Transform eyes, sight;
    public AudioClip clip;
    GunEffect visualEffect;
    public GameObject explotionEffect;
    float gunPower = 460f;
    NavMeshAgent agent;
    Animator anim;
    private void Awake()
    {
        base.Start();
        blood = 100f;
        eyes = GetComponentInChildren<Eyes>().transform;
        sight = GetComponentInChildren<Sight>().transform;
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
    }
    void Update()
    {
        base.Update();
        RaycastHit hit;
        if (Physics.Raycast(eyes.transform.position, eyes.TransformDirection(Vector3.forward), out hit, 200f))
        {
            if (hit.transform.GetComponent<Gatto>())
            {
                Debug.Log(this.transform.name + " otro gatto");
                onFirstGattoSight.Invoke();
                IsSeeingGatto = true;
            }
            else
            {
                IsSeeingGatto = false;
            }
            Debug.DrawRay(eyes.transform.position, eyes.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
        }
    }
    public override void AttackEffect()
    {
        PerformAttack();
        visualEffect = GetComponentInChildren<GunEffect>();
        base.AttackEffect();
        visualEffect.Playeffect();
        AudioSource aSource = GetComponent<AudioSource>();
        SoundManager.main.AudioPlay(aSource, clip);
    }

    private void PerformAttack()
    {
        RaycastHit hit;
        if (Physics.Raycast(sight.transform.position, sight.transform.TransformDirection(Vector3.forward), out hit, 1f))
        {
            Debug.DrawRay(sight.transform.position, sight.transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            if (hit.transform.GetComponent<Gatto>())
            {
                hit.transform.GetComponent<Gatto>().GetDamaged(gunPower, 0.5f);
            }
        }
    }
    public override void Attack(Gatto gatto)
    {
        agent.destination = gatto.transform.position;
        agent.speed = 5.5f;
        anim.speed = 2.3f;
    }
    public override void Hide(Gatto gatto)
    {

        agent.stoppingDistance = 1f;
        agent.destination = this.nearestPanel.transform.position;
        if (Vector3.Distance(agent.velocity, Vector3.zero) < 0.5f)
        {
            agent.transform.LookAt(gatto.transform);
            anim.SetBool("IsStopped", true);
        }
        else
        {
            anim.SetBool("IsStopped", false);
        }
    }
    public override void Threat(Gatto gatto)
    {

    }
    public override void InitSearch()
    {
        Debug.Log("se ejecuta");
        agent.speed = 2.5f;
        anim.speed = 1.8f;
        agent.GetComponentInChildren<GraphicState>().gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.yellow);

    }
    public override void InitPatrol()
    {
        agent = GetComponent<NavMeshAgent>();
        GetComponent<Animator>().speed = 1f;
        agent.speed = 0.9f;
        agent.gameObject.GetComponent<Animator>().SetFloat("AgentVelocity", 1);
        //agent.gameObject.GetComponent<Animator>().Play("idle");
        agent.GetComponentInChildren<GraphicState>().gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.blue);

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Gatto>())
        {
            if (this.CurrState() is EAttack)
            {
                agent.Stop();
                other.GetComponent<Gatto>().GetDamaged(120, 0.5f);
                //other.GetComponent<Rigidbody>().AddForce(transform.forward.normalized * 660f);
                blood = 0;
            }
        }
    }
    public override void DeadEffect()
    {
        MonoBehaviour.Destroy(this.gameObject);
        GameObject TempExplotion;
        TempExplotion = Instantiate(explotionEffect, transform.position + (transform.forward * 0.1f), transform.rotation);
        Destroy(TempExplotion, 8F);
    }
}
