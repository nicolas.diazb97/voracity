﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Chanda : Enemy
{
    public Transform eyes, sight;
    public AudioClip clip;
    GunEffect visualEffect;
    float gunPower = 460f;
    NavMeshAgent agent;
    Animator anim;
    private void Start()
    {
        base.Start();
        blood = 100f;
        eyes = GetComponentInChildren<Eyes>().transform;
        sight = GetComponentInChildren<Sight>().transform;
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
    }
    void Update()
    {
        base.Update();
        RaycastHit hit;
        if (Physics.Raycast(eyes.transform.position, eyes.TransformDirection(Vector3.forward), out hit, 200f))
        {
            if (hit.transform.GetComponent<Gatto>())
            {
                Debug.Log(this.transform.name + " otro gatto");
                onFirstGattoSight.Invoke();
                IsSeeingGatto = true;
            }
            else
            {
                IsSeeingGatto = false;
            }
            Debug.DrawRay(eyes.transform.position, eyes.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
        }
    }
    public override void AttackEffect()
    {
        PerformAttack();
        visualEffect = GetComponentInChildren<GunEffect>();
        base.AttackEffect();
        visualEffect.Playeffect();
        AudioSource aSource = GetComponent<AudioSource>();
        SoundManager.main.AudioPlay(aSource, clip);
    }

    private void PerformAttack()
    {
        RaycastHit hit;
        if (Physics.Raycast(sight.transform.position, sight.transform.TransformDirection(Vector3.forward), out hit, 100f))
        {
            Debug.DrawRay(sight.transform.position, sight.transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            if (hit.transform.GetComponent<Gatto>())
            {
                hit.transform.GetComponent<Gatto>().GetDamaged(gunPower, 0.5f);
            }
        }
    }
    public override void Attack(Gatto gatto)
    {
        agent.stoppingDistance = 11f;
        agent.destination = gatto.transform.position;
        if (this.GattoIsAiming)
        {
            agent.Stop();
            agent.velocity = Vector3.zero;
            anim.SetBool("IsStopped", true);
            agent.speed = 8f;
            anim.speed = 3f;
            agent.GetComponentInChildren<GraphicState>().gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.magenta);
            this.GattoIsAiming = false;
        }
        else
        {
            agent.Resume();
            agent.GetComponentInChildren<GraphicState>().gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.red);
            agent.speed = 2f;
            anim.speed = 1.3f;
        }
        var rotation = Quaternion.LookRotation(gatto.transform.position - agent.transform.position);
        agent.transform.rotation = Quaternion.Slerp(agent.transform.rotation, rotation, Time.deltaTime * 4F);
        if (Vector3.Distance(agent.velocity, Vector3.zero) < 0.05f)
        {
            anim.SetBool("IsStopped", true);
        }
        else
        {
            anim.SetBool("IsStopped", false);
        }


    }
    public override void Hide(Gatto gatto)
    {
        agent.stoppingDistance = 1f;
        agent.destination = this.nearestPanel.transform.position;
        if (Vector3.Distance(agent.velocity, Vector3.zero) < 0.5f)
        {
            agent.transform.LookAt(gatto.transform);
            anim.SetBool("IsStopped", true);
        }
        else
        {
            anim.SetBool("IsStopped", false);
        }

    }
    public override void Threat(Gatto gatto)
    {

    }
    public override void InitSearch()
    {
        int randomSearch = UnityEngine.Random.Range(0, 10);
        Debug.Log(randomSearch);
        if (randomSearch < 5)
        {
            agent.gameObject.GetComponent<Animator>().SetFloat("AgentVelocity", 1.8f);
        }
        else
        {
            agent.speed = 2.5f;
            agent.gameObject.GetComponent<Animator>().speed = 1.8f;
        }
        agent.GetComponentInChildren<GraphicState>().gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.yellow);

    }
    public override void InitPatrol()
    {
        agent = GetComponent<NavMeshAgent>();
        this.gameObject.GetComponent<Animator>().speed = 1f;
        agent.speed = 0.9f;
        agent.gameObject.GetComponent<Animator>().SetFloat("AgentVelocity", 1);
        agent.GetComponentInChildren<GraphicState>().gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.blue);
    }
    public override void DeadEffect()
    {
        this.gameObject.GetComponent<Animator>().speed = 1f;
        agent.speed = 0.9f;
        anim.Play("death");
        Collider[] colliders;
        colliders =GetComponents<Collider>();
        for (int i = 0; i< colliders.Length;i++)
        {

            MonoBehaviour.Destroy(colliders[i]);
        }
        MonoBehaviour.Destroy(this.gameObject.GetComponent<NavMeshAgent>());
        MonoBehaviour.Destroy(this.gameObject.GetComponent<Enemy>());
        MonoBehaviour.Destroy(this.gameObject, 5f);
    }
}
