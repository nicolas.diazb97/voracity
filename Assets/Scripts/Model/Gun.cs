﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Gun : Attack
{
    public override AttackStats Stats { get; set; }
    public AudioClip clip;
    AudioSource aSource;
    bool shooting = false, bursting = false, aimZoom = false;
    float distanceToZoom = 1.5f, heightToZoom = 2.0f;
    public GameObject bulletHole;
    public GameObject blood;
    public GameObject muzzle;
    bool OnAttack = false;
    Animator anim2;

    GameObject sight;
    float gunPower = 80f;
    void Start()
    {
        sight = GetComponentInChildren<Sight>().gameObject;
        aSource = GetComponent<AudioSource>();
        Stats = new AttackStats(1.07f, 80, 2, 40);
    }
    public override void PerformAttack(Animator anim)
    {
        attacking = true;
        bursting = (shooting) ? true : false;
        shooting = true;
        OnAttack = true;//
        anim2 = anim;//
        anim.SetBool("OnAiming", true);
        SoundManager.main.AudioPlay(aSource, clip);
        StartCoroutine(CheckOnBurst());
        AffectOther();
        GameObject tempMuzzle;
        tempMuzzle = Instantiate(muzzle, sight.transform.position, sight.transform.rotation);
        tempMuzzle.transform.SetParent(this.transform);
        Destroy(tempMuzzle, 3f);
    }
    IEnumerator CheckOnBurst()
    {
        yield return new WaitForSecondsRealtime(0.3f);
        //Debug.Log(bursting + " " + shooting);
        if (!bursting)
        {
            //Debug.Log("llegue");
            shooting = false;
            anim2.SetBool("OnAiming", false);
        }
        bursting = false;
    }
    private void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.TransformDirection(Vector3.forward), out hit, 100f))
        {
            if (hit.transform.GetComponent<Enemy>())
            {
                hit.transform.GetComponent<Enemy>().GattoIsAiming = true;
            }
            Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
        }
    }
    private void AffectOther()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.TransformDirection(Vector3.forward), out hit, 100f))
        {
            //if (hit.transform.GetComponent<Gatto>())
            //{
            //    Physics.IgnoreRaycastLayer
            //}
            Debug.Log(hit.transform.name);
            if (hit.transform.GetComponent<Enemy>())
            {
                GameObject TempBlood;
                TempBlood = Instantiate(blood, hit.point + (hit.normal * 0.00000000001f), Quaternion.Euler(hit.transform.position));
                Destroy(TempBlood, 3f);
                if (hit.transform.GetComponent<Enemy>().CurrState() is EDead) { }
                else
                {

                }
                hit.transform.GetComponent<Enemy>().GetDamaged(gunPower, 0.5f);
            }
            if (hit.transform.GetComponent<Ratota>())
            {
                hit.transform.GetComponent<Enemy>().SetState(new EAttack());
            }
            if (hit.transform.GetComponent<Rigidbody>())
            {
                hit.transform.GetComponent<Rigidbody>().AddForce(-hit.normal * 460f);
            }
            else
            {
                //if (hit.transform.GetComponent<WallHole>())
                //{
                GameObject TempBulletHole;
                TempBulletHole = Instantiate(bulletHole, hit.point + (hit.normal * 0.1f), Quaternion.Euler(hit.transform.position));
                TempBulletHole.transform.SetParent(hit.transform);
                Destroy(TempBulletHole, 1F);
                TempBulletHole.transform.rotation = Quaternion.LookRotation(hit.normal);
                //}
            }
        }
    }
    public override float GetWeight()
    {
        return Stats.weight;
    }

    public override GameObject GetGameObject()
    {
        return gameObject;
    }

    public override void TryToAim(bool state, Animator anim)
    {
        anim.SetBool("OnAiming", state);
        Camera.main.GetComponent<vThirdPersonCamera>().defaultDistance = (state) ? distanceToZoom : 2.97f;
        Camera.main.GetComponent<vThirdPersonCamera>().height = (state) ? heightToZoom : 2.02f;
        aimZoom = true;
    }
}
