﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

using Invector.CharacterController;
public class Enemy : MonoBehaviour
{
    public RoutePoint[] pointsPatrol;
    public RoutePoint[] pointsSearch;
    public GameObject routePatrol;
    public GameObject routeSearch;
    public UnityEvent onFirstGattoSight;
    public UnityEvent onDeadEnemy;
    public bool IsSeeingGatto;
    public bool GattoIsAiming;
    public float chanceToShoot;
    public Transform nearestPanel;
    Attack attack;
    float vulnerable;
    public float blood;
    int destPoint = 0;
    protected NavMeshAgent agent;
    IEnemyState state;
    Gatto gatto;
    protected virtual void Start()
    {
        gatto = FindObjectOfType<Gatto>();
        pointsPatrol = routePatrol.GetComponentsInChildren<RoutePoint>();
        pointsSearch = routeSearch.GetComponentsInChildren<RoutePoint>();
        StartCoroutine(Decide(gameObject.GetComponent<Animator>()));
        CharacterInit();
        //this.Init();
    }
    public virtual void GetDamaged(float gunPower, float BodyPartMultiplier)
    {
        onFirstGattoSight.Invoke();
        agent.gameObject.GetComponent<Animator>().Play("Damaged");
        blood -= gunPower * BodyPartMultiplier;
    }
    public IEnemyState CurrState()
    {
        return state;
    }
    public virtual void DeadEffect()
    {

    }
    public virtual void AttackEffect()
    {

    }
    public virtual void Attack(Gatto gatto)
    {

    }
    public virtual void Hide(Gatto gatto)
    {

    }
    public virtual void Threat(Gatto gatto)
    {

    }
    public virtual void InitSearch()
    {

    }
    public virtual void InitPatrol()
    {

    }
    public float DistanceTo(Transform t)
    {
        return Vector3.Distance(gameObject.transform.position, t.position);
    }
    protected virtual void CharacterInit()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.autoBraking = false;
        this.SetState(new EPatrol());
    }


    public void SetState(IEnemyState nState)
    {
        //if (state is Object)
        //    state.StopMyCoroutine();
        state = nState;
        state.Init(agent);
    }
    protected virtual void LateUpdate()
    {
        //this.input.y = agent.speed;
        //agent.SetDestination(tr_Player.position);
        if (blood <= 0)
        {
            this.SetState(new EDead());
            onDeadEnemy.Invoke();
        }
    }
    IEnumerator Decide(Animator _currEnemyAnim)
    {
        Animator currEnemyAnim = _currEnemyAnim;
        currEnemyAnim.SetBool("DecideToShoot", Shoot(chanceToShoot));
        yield return new WaitForSecondsRealtime(1f);
        StartCoroutine(Decide(currEnemyAnim));
    }
    bool Shoot(float probability)
    {
        return
        Random.Range(0.00f, 1.00f) < probability;
    }
    protected virtual void Update()
    {

        state.Process(agent, gatto);
        //if (!agent.pathPending && agent.remainingDistance < 0.5f)
        //    GotoNextPointPatrol();

    }

}
