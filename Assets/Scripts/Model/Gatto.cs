﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
public class Gatto : MonoBehaviour
{

    // Attack attack;
    float blood = 300;
    int stamina;
    int damageCounter = 0;
    public MyAttackEvent onTryEquipWeapon;
    public MyAnimatorEvent onPerfomAttack;

    internal void AddLife(int bloodToAdd)
    {
        blood += bloodToAdd;
        if (blood > 300)
        {
            blood = 300;
        }
        bloodUI.fillAmount = blood.Remap(0, 300, 0, 1);
    }

    public TextMeshProUGUI health;
    public OnCollectEvent onCollect;
    public AimEvent OnAiming;
    public Animator bloodAnim;
    public GameObject popUpPanel;
    bool alive = true;
    public Image bloodUI;
    public GameObject skinMesh;
    Text panelText;
    Animator anim;
    public Material electricMat;

    private void Start()
    {
        anim = GetComponent<Animator>();
        panelText = popUpPanel.GetComponentInChildren<Text>();
    }
    public bool isEquipped { get; internal set; }

    public void EquipWeapon(ICollectable item)
    {
        if (item is IAttack)
            onTryEquipWeapon.Invoke(item as IAttack);
    }

    public void PerfomAttack(Animator anim)
    {
        if (alive)
        {
            onPerfomAttack.Invoke(anim);
        }
    }

    public void Collect()
    {
        if (alive)
        {
            onCollect.Invoke(gameObject.transform);
        }
    }
    public void TryToAim(bool state, Animator anim)
    {
        if (alive)
        {
            OnAiming.Invoke(state, anim);
        }
    }
    /*
    void DoAttack(){

    }

    void SetAttack (Attack _a){
    attack = _a;
    }

    void Run(){

    }

    void AddStamina(int _s){

    }
    */
    public void LoadStats(IAttack attack)
    {

    }
    public void ReleaseStats(IAttack attack)
    {

    }

    public void GetDamaged(float gunPower, float BodyPartMultiplier)
    {
        bloodAnim.SetBool("OnBlood", true);
        damageCounter++;
        health.text = blood.ToString();
        if (alive&& gunPower >=460)
        {
            anim.Play("Damaged");
        }
        blood -= gunPower * BodyPartMultiplier;
        if (blood <= 0)
        {
            //OnDeath();
        }
        StartCoroutine(CheckforBurst());
        bloodUI.fillAmount = blood.Remap(0, 300, 0, 1);
    }
    public void GetDamagedByElectricBullet(float gunPower, float BodyPartMultiplier)
    {
        bloodAnim.SetBool("OnBlood", true);
        damageCounter++;
        health.text = blood.ToString();
        if (alive)
        {
            anim.Play("Damaged");
        }
        blood -= gunPower * BodyPartMultiplier;
        if (blood <= 0)
        {
            OnDeath();
        }
        Material[] mats = new Material[] {
        skinMesh.GetComponent<SkinnedMeshRenderer>().materials[0],
        skinMesh.GetComponent<SkinnedMeshRenderer>().materials[1],
        electricMat
        };
        skinMesh.GetComponent<SkinnedMeshRenderer>().materials = mats;
        StartCoroutine(CheckforBurst());
        bloodUI.fillAmount = blood.Remap(0, 300, 0, 1);
    }
    IEnumerator WaitToUnElectric()
    {
        yield return new WaitForSecondsRealtime(2f);

    }
    void OnDeath()
    {
        alive = false;
        anim.Play("death");
        popUpPanel.gameObject.SetActive(true);
        panelText.text = "You failed! Loading checkpoint...";
        StartCoroutine(WaitToDeath());
    }
    IEnumerator WaitToDeath()
    {
        yield return new WaitForSecondsRealtime(4f);
        SceneManager.LoadScene(0);
    }
    IEnumerator CheckforBurst()
    {
        yield return new WaitForSecondsRealtime(2f);
        if (damageCounter <= 1)
        {
            damageCounter = 0;
            bloodAnim.SetBool("OnBlood", false);
        }
        damageCounter--;
    }
    public void Hide()
    {
        anim.SetBool("Hide", true);
        anim.Play("cubrirse");
        this.transform.rotation = Camera.main.transform.rotation;
    }
    public void OffHide()
    {
        anim.SetBool("Hide", false);
    }

}
public static class ExtensionMethods
{

    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

}
