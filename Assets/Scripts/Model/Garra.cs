﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Garra : MonoBehaviour
{
    //public override AttackStats Stats { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

    //public override void PerformAttack(Animator anim)
    //{
    //    Debug.Log("garra");
    //    anim.SetBool("Garra", true);
    //    StartCoroutine(ShowColliders());
    //    StartCoroutine(WaitToStopAnim(anim));
    //}
    public IEnumerator WaitToStopAnim(Animator anim)
    {
        yield return new WaitForSecondsRealtime(1f);
        anim.SetBool("Garra", false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
           // other.GetComponent<IEnemy>().TakeDamage(stats[0].GetCalculatedStatValue());
        }
    }
    public IEnumerator ShowColliders()
    {
        for (int i = 0; i < GetComponents<BoxCollider>().Length; i++)
        {
            GetComponents<BoxCollider>()[i].enabled = true;
        }
        yield return new WaitForSecondsRealtime(2f);
        HideColliders();
    }
    public void HideColliders()
    {
        for (int i = 0; i < GetComponents<BoxCollider>().Length; i++)
        {
            GetComponents<BoxCollider>()[i].enabled = false;
        }
    }

    //public override float GetWeight()
    //{
    //    return 0;
    //}

    //public override GameObject GetGameObject()
    //{
    //    return gameObject;
    //}


}
