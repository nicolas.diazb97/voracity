﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HidePanel : MonoBehaviour
{
    public bool IsOcuppied = false;
    public float DistanceTo(Transform t)
    {
        return Vector3.Distance(gameObject.transform.position, t.position);
    }
    public GameObject GetGameObject()
    {
        return this.gameObject;
    }
}
