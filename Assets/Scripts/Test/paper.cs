﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class paper : MonoBehaviour
{
    // Start is called before the first frame update
    public Rigidbody RB;
    void Start()
    {
        RB = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag=="Gatto")
        {
            RB.AddForce(0, 100, 200, ForceMode.Acceleration);
        }
    }
}
