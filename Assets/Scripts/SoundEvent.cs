﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundEvent : MonoBehaviour
{
    public AudioClip clip;
    AudioSource audioSource;
    public void PlaySound()
    {
        audioSource.PlayOneShot(clip);
    }
}
