﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public interface IAttack :ICollectable
    {
    AttackStats Stats { get; set; }
    string name { get; set; }
    void PerformAttack(Animator anim);
    void TryToAim(bool state, Animator anim);
    float GetWeight();
    void SetPhysics(bool state);
    bool CheckAttacking();
}
