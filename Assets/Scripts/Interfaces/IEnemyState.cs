﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public interface IEnemyState
{
    void Process(NavMeshAgent agent, Gatto gatto);
    void Init(NavMeshAgent agent);
}
