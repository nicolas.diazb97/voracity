﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    public List<BaseStat> stats = new List<BaseStat>();

    void Awake()
    {
        stats.Add(new BaseStat(4, "Power-Gun", "New Power Level"));
        stats.Add(new BaseStat(4, "Power-Garra", "New Power Level"));
        //stats[0].AddStatBonus(new StatBonus(5));
        //stats[0].AddStatBonus(new StatBonus(-7));
        //stats[0].AddStatBonus(new StatBonus(21));
        //Debug.Log(stats[0].GetCalculatedStatValue());
    }
    public void AddStatBonus(List<BaseStat> statBonuses)
    {
        foreach (BaseStat statBonus in statBonuses)
        {
            stats.Find(x=> x.StatName == statBonus.StatName).AddStatBonus(new StatBonus(statBonus.BaseValue));
            Debug.Log(stats.Find(x => x.StatName == statBonus.StatName));
        }
    }
    public void RemoveStatBonus(List<BaseStat> statBonuses)
    {
        foreach (BaseStat statBonus in statBonuses)
        {
            stats.Find(x => x.StatName == statBonus.StatName).RemoveStatBonus(new StatBonus(statBonus.BaseValue));
        }
    }
}
