﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
[RequireComponent(typeof(Animator))]
public class PicasFijas : Puzzle {
    int[] options = new int[] { 1, 2, 3, 4, 5 };
    // Start is called before the first frame update    public List<DigitPF> digits;
    List<DigitPF> digits;
    DigitPF actualSelected;
    List<int> response = new List<int>();
    public string R;
    public UnityEvent onInitCheckResponse;
    public UnityEvent onGuest;
    bool allSet = false;

    void Start() {
        digits = GetComponentsInChildren<DigitPF>().ToList();
        onInitCheckResponse.AddListener(() => {
            this.enabled = false;
            gameObject.GetComponent<Animator>().enabled = true;
            gameObject.GetComponent<Animator>().PlayInFixedTime("checking", -1, 0f);
        });
        Setup();
    }

    public void Setup() {
        actualSelected = digits.OrderBy(d => d.position).Last();
        digits.ForEach(d => d.Init(options));
        ActivateDigit(actualSelected.position);
        TurnOffresponse();
        response.Clear();
        do {
            response.Add(options[UnityEngine.Random.Range(0, options.Length)]);
            response = response.Distinct().ToList();
        } while (response.Count < digits.Count);
        R = System.String.Join("", response);
    }

    public void OnEndCheckResponse() {
        this.enabled = true;
    }
    private void TurnOffresponse() {
        digits.ForEach(d => d.TurnOffResponse());
    }

    private void ActivateDigit(int toSel) {
        digits.ForEach(d => d.SetOpacity(d.position == toSel));
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.UpArrow))
            actualSelected.Increase(digits.Where(d => d != actualSelected).ToList());
        if (Input.GetKeyDown(KeyCode.DownArrow))
            actualSelected.Decrease(digits.Where(d => d != actualSelected).ToList());
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            ChangeSelectedIn(+1);
        if (Input.GetKeyDown(KeyCode.RightArrow))
            ChangeSelectedIn(-1);
        allSet = digits.All(d => d.ixActual >= 0);
        if ((Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter)) && allSet)
            CheckResponse();
    }

    private void ChangeSelectedIn(int offset) {
        if (actualSelected.position > 0 && offset < 0 || actualSelected.position < digits.Count - 1 && offset > 0) {
            digits.Where(d => d != actualSelected).ToList().ForEach(d => d.StayOrReset(actualSelected.ixActual));
            actualSelected = digits.ElementAt(actualSelected.position + offset);
            ActivateDigit(actualSelected.position);
        }
    }
    private void CheckResponse() {
        digits.Where(d => d != actualSelected).ToList().ForEach(d => d.StayOrReset(actualSelected.ixActual));
        if (!digits.All(d => d.ixActual >= 0))
            return;
        onInitCheckResponse.Invoke();
        int picas = 0;
        int fijas = 0;
        digits.ForEach(d => { fijas += d.CheckResponse(response)[0] ? 1 : 0; });
        digits.ForEach(d => { picas += d.CheckResponse(response)[1] ? 1 : 0; });
        digits.OrderByDescending(d => d.position).Take(picas).ToList().ForEach(d => d.EnablePica());
        digits.OrderByDescending(d => d.position).Take(fijas).ToList().ForEach(d => d.EnableFija());
        if (fijas == digits.Count)
            onGuest.Invoke();
    }

}
