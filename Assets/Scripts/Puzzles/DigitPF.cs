﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DigitPF : MonoBehaviour {
    public int position;
    // Start is called before the first frame update
    GameObject number;
    GameObject pica;
    GameObject fija;
    GameObject clue;
    int[] options;
    public int ixActual { private set; get; }
    Color enablePica;
    Color enableFija;
    Color disablePica;
    Color disableFija;
    void Awake() {
        GetComponentsInChildren<Transform>().ToList().ForEach(f => {
            switch (f.name) {
                case "number":
                    number = f.gameObject;
                    break;
                case "pica":
                    pica = f.gameObject;
                    enablePica = new Color(pica.GetComponent<Image>().color.r, pica.GetComponent<Image>().color.g, pica.GetComponent<Image>().color.b, 1f);
                    disablePica = new Color(pica.GetComponent<Image>().color.r, pica.GetComponent<Image>().color.g, pica.GetComponent<Image>().color.b, 0.2f);
                    break;
                case "fija":
                    fija = f.gameObject;
                    enableFija = new Color(fija.GetComponent<Image>().color.r, fija.GetComponent<Image>().color.g, fija.GetComponent<Image>().color.b, 1f);
                    disableFija = new Color(fija.GetComponent<Image>().color.r, fija.GetComponent<Image>().color.g, fija.GetComponent<Image>().color.b, 0.2f);
                    break;
                case "clue":
                    clue = f.gameObject;
                    break;
            }
        });
    }


    internal void Init(int[] o) {
        options = o;
        ixActual = -1;
        number.GetComponent<Text>().text = "-";
        clue.GetComponent<Image>().enabled = false;
    }

    internal void StayOrReset(int _ix) {
        if (_ix == ixActual)
            Init(options);
    }

    internal void Increase(List<DigitPF> others) {
        // do 
        ixActual = GetNextixActual(ixActual);
        // while (others.Any(o => o.ixActual == ixActual));
        UpdateScreen(others);
    }

    internal void Decrease(List<DigitPF> others) {
        // do 
        ixActual = GetPreviousixActual(ixActual);
        // while (others.Any(o => o.ixActual == ixActual));
        UpdateScreen(others);
    }
    private void UpdateScreen(List<DigitPF> others) {
        number.GetComponent<Text>().text = options[ixActual].ToString();
        GetComponentsInChildren<Image>().ToList().Where(i => i.name != "arrow").ToList().ForEach(e => e.enabled =false);
        others.ForEach(o => {
            o.fija.GetComponent<Image>().enabled = false;
            o.pica.GetComponent<Image>().enabled = false;
        });
        // fija.GetComponent<Image>().color = disableFija;
        // pica.GetComponent<Image>().color = disablePica;
    }

    private int GetNextixActual(int ixActual) {
        return ixActual + 1 >= options.Length ? 0 : ixActual + 1;
    }


    internal bool[] CheckResponse(List<int> attempts) {
        if (ixActual < 0)
            return new bool[2];
        bool[] response = new bool[2];
        response[0] = attempts.ElementAt(attempts.Count - 1 - position) == options[ixActual];
        response[1] = attempts.Exists(e => e == options[ixActual]) && !response[0];
        fija.GetComponent<Image>().enabled = true;
        pica.GetComponent<Image>().enabled = true;
        pica.GetComponent<Image>().color = disablePica;
        fija.GetComponent<Image>().color = disableFija;
        return response;
    }

    internal void EnablePica() {
        pica.GetComponent<Image>().color = enablePica;
    }

    internal void EnableFija() {
        fija.GetComponent<Image>().color = enableFija;
    }

    private int GetPreviousixActual(int ixActual) {
        return ixActual <= 0 ? options.Length - 1 : ixActual - 1;
    }

    internal void SetOpacity(bool v) {
        Color tc = GetComponentInChildren<Text>().color;
        GetComponentInChildren<Text>().color = new Color(tc.r, tc.g, tc.b, v ? 1f : 0.7f);
        number.GetComponentsInChildren<Image>().ToList().ForEach(i => i.enabled = v);
    }
    internal void TurnOffResponse() {
        // fija.GetComponent<Image>().color = disableFija;
        // pica.GetComponent<Image>().color = disablePica;
        GetComponentsInChildren<Image>().ToList().Where(i => i.name != "arrow").ToList().ForEach(e => e.enabled =false);
    }


}
