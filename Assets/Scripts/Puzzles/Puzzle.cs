﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puzzle : MonoBehaviour
{
    // Start is called before the first frame update
    public Text numbers;
    public PuzzleDoor door;
    public void OpenDoor()
    {
        door.UnlockDoor();
    }
}
