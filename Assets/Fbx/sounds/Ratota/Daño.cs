﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Daño : MonoBehaviour
{
    public AudioClip SoundToPlay;
    public AudioSource source;


    void Awake()
    {

        source = GetComponent<AudioSource>();
    }

    void playSound()
    {

        source.PlayOneShot(SoundToPlay);
    }
}
