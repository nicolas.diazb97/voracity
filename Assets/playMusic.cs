﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playMusic : MonoBehaviour
{
    public AudioClip musicTuneles;
    bool once = true;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Gatto>() && once)
        {
            MusicManager.main.PlayTrack(musicTuneles);
            once = false;

        }
    }
}
