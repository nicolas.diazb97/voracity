﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivateFinalImage : Singleton<ActivateFinalImage>
{
    public void Activate()
    {
        this.GetComponent<Image>().enabled = true;
    }

}
