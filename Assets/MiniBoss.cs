﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MiniBoss : Enemy
{
    public Transform eyes, sight;
    public AudioClip clip;
    public GameObject bloodParticle;
    GunEffect visualEffect;
    float gunPower = 48f;
    NavMeshAgent agent;
    Animator anim;
    public GameObject bossInitial;
    GameObject punchCollider;
    private void Start()
    {
        base.Start();
        punchCollider = GetComponentInChildren<punch>().gameObject;
        punchCollider.SetActive(false);
        blood = 600f;
        eyes = GetComponentInChildren<Eyes>().transform;
        sight = GetComponentInChildren<Sight>().transform;
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
    }
    void Update()
    {
        base.Update();
        RaycastHit hit;
        if (Physics.Raycast(eyes.transform.position, eyes.TransformDirection(Vector3.forward), out hit, 200f))
        {
            if (hit.transform.GetComponent<Gatto>())
            {
                Debug.Log(this.transform.name + " otro gatto");
                onFirstGattoSight.Invoke();
                IsSeeingGatto = true;
            }
            else
            {
                IsSeeingGatto = false;
            }
            Debug.DrawRay(eyes.transform.position, eyes.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
        }
    }
    public override void AttackEffect()
    {
        PerformAttack();
        visualEffect = GetComponentInChildren<GunEffect>();
        base.AttackEffect();
        visualEffect.Playeffect();
        AudioSource aSource = GetComponent<AudioSource>();
        SoundManager.main.AudioPlay(aSource, clip);
    }

    private void PerformAttack()
    {
        RaycastHit hit;
        if (Physics.Raycast(sight.transform.position, sight.transform.TransformDirection(Vector3.forward), out hit, 100f))
        {
            Debug.DrawRay(sight.transform.position, sight.transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            if (hit.transform.GetComponent<Gatto>())
            {
                hit.transform.GetComponent<Gatto>().GetDamaged(gunPower, 0.5f);
            }
        }
    }
    public override void Attack(Gatto gatto)
    {
        //    agent.Stop();
        //    agent.velocity = Vector3.zero;
        //    anim.SetBool("IsStopped", true);
        //    agent.speed = 4f;
        //    anim.speed = 2f;
        //    agent.GetComponentInChildren<GraphicState>().gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.magenta);
        //    this.GattoIsAiming = false;

        var rotation = Quaternion.LookRotation(gatto.transform.position - agent.transform.position);
        agent.transform.rotation = Quaternion.Slerp(agent.transform.rotation, rotation, Time.deltaTime * 4F);
        if (Vector3.Distance(agent.velocity, Vector3.zero) < 0.05f)
        {
            anim.SetBool("IsStopped", true);
        }
        else
        {
            anim.SetBool("IsStopped", false);
        }
        
        if (Vector3.Distance(bossInitial.transform.position, gatto.transform.position) < 8f)
        {
            Debug.Log("resume");
            agent.Resume();
            agent.SetDestination(gatto.transform.position);
            if (Vector3.Distance(transform.position, gatto.transform.position) < 3f)
            {
                Debug.Log("stopping");
                punchCollider.SetActive(true);
                anim.Play("melee");
                agent.Stop();
            }
            else
            {
               punchCollider.SetActive(false);
            }
        }
        else
        {
            agent.Resume();
            Debug.Log("devolverse");
            agent.SetDestination(bossInitial.transform.position);
            if(Vector3.Distance(bossInitial.transform.position, transform.position) < 1f)
            {
                Debug.Log("devuelto");
                agent.Stop();
            }
        }
    }
    public void DamagedEffect()
    {
        bloodParticle.SetActive(true);
    }
    public void DamagedEffectOff()
    {
        bloodParticle.SetActive(false);
    }
    public override void Hide(Gatto gatto)
    {
        agent.Stop();
        //agent.stoppingDistance = 1f;
        //agent.destination = this.nearestPanel.transform.position;
        //if (Vector3.Distance(agent.velocity, Vector3.zero) < 0.5f)
        //{
        //    agent.transform.LookAt(gatto.transform);
        //    anim.SetBool("IsStopped", true);
        //}
        //else
        //{
        //    anim.SetBool("IsStopped", false);
        //}

    }
    public override void Threat(Gatto gatto)
    {

    }
    public override void InitSearch()
    {
        agent.Stop();
        //int randomSearch = UnityEngine.Random.Range(0, 10);
        //Debug.Log(randomSearch);
        //if (randomSearch < 5)
        //{
        //    agent.gameObject.GetComponent<Animator>().SetFloat("AgentVelocity", 1.8f);
        //}
        //else
        //{
        //    agent.speed = 2.5f;
        //    agent.gameObject.GetComponent<Animator>().speed = 1.8f;
        //}
        //agent.GetComponentInChildren<GraphicState>().gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.yellow);

    }
    public override void InitPatrol()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.Stop();
        //agent.gameObject.GetComponent<Animator>().;
        //agent.speed = 0.9f;
        //agent.gameObject.GetComponent<Animator>().SetFloat("AgentVelocity", 1);
        //agent.GetComponentInChildren<GraphicState>().gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.blue);

    }
    public override void DeadEffect()
    {
        agent.speed = 8f;
        anim.speed = 3f;
        anim.Play("death");
        MonoBehaviour.Destroy(this.gameObject, 5f);
        MonoBehaviour.Destroy(this.gameObject.GetComponent<Enemy>());
        Collider[] colliders;
        colliders = GetComponents<Collider>();
        for (int i = 0; i < colliders.Length; i++)
        {

            MonoBehaviour.Destroy(colliders[i]);
        }
        MonoBehaviour.Destroy(this.gameObject.GetComponent<NavMeshAgent>());
    }

}