﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricRatota : MonoBehaviour
{
    public GameObject ray;
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        ray.SetActive(true);
    }
    private void OnTriggerExit(Collider other)
    {
        ray.SetActive(false);
    }
}
