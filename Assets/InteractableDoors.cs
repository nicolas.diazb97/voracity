﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableDoors : Singleton<InteractableDoors>
{
   
    public void PlayAnimation(Animator anim)
    {
        anim.Play("Open");
    }
}
