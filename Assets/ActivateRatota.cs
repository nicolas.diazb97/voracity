﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateRatota : MonoBehaviour
{
    public GameObject UI;
    public AudioClip musicFinalfight;
    bool once = true;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Gatto>() && once)
        {
            UI.SetActive(true);
            MusicManager.main.PlayFinishedTrack(musicFinalfight);
            MusicManager.main.source.loop = true;
            once = false;

        }
    }
}
