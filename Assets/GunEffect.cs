﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunEffect : MonoBehaviour
{
    Light ligth;
    // Start is called before the first frame update
    void Start()
    {
        ligth = GetComponentInChildren<Light>(true);
        ligth.enabled = false;
    }

    internal void Playeffect()
    {
        ligth.enabled = true;
        StartCoroutine(ShutDownEffect());
    }

    private IEnumerator ShutDownEffect()
    {
        yield return new WaitForSecondsRealtime(0.05f);
        ligth.enabled = false;
    }
}
