﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateTroop : MonoBehaviour
{
    public TroopController currTroop, nextTroop;
    bool once = true;
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (currTroop.enemiesClear && once)
        {
            if (other.GetComponent<Gatto>())
            {
                once = false;
                nextTroop.gameObject.SetActive(true);
            }
        }
    }
}
