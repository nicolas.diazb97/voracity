﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleDoor : MonoBehaviour
{
    public TroopController currTroopOnAttack;
    public TroopController nextTroop;
    public bool locked;
    public bool cinematic;
    public int clipId;
    public bool actionAfter;
    bool once = true;
    public GameObject objToActivate;
    public AudioClip trackToPlay;
    public GameObject nextPuzzle;
    public bool triggerDoor;
    private void OnTriggerEnter(Collider other)
    {        
        if (other.GetComponent<Gatto>() && once && !locked&&triggerDoor&&currTroopOnAttack.enemiesClear)
        {
            UnlockDoor();
            once = false;
        }
    }
    private void Ontriggerenter(Collider other)
    {
    }
    public void UnlockDoor()
    {
        nextPuzzle.SetActive(true);
        GetComponent<BoxCollider>().enabled = false;
        if (cinematic)
        {
            CinematicManager.main.PlayVideo(clipId);
        }
        InteractableDoors.main.PlayAnimation(GetComponent<Animator>());
        currTroopOnAttack.gameObject.SetActive(false);
    }
    public void OpenDoor()
    {
        MusicManager.main.PlayTrack(trackToPlay);
        if (actionAfter)
        {
            objToActivate.SetActive(true);
        }
        nextTroop.gameObject.SetActive(true);
    }
}
