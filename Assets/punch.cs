﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class punch : MonoBehaviour
{
    private Gatto gatto;
    float shootPower = 480f;
    // Start is called before the first frame update
    void Start()
    {

        gatto = FindObjectOfType<Gatto>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Gatto>())
        {
            gatto.GetDamaged(shootPower, 0.5f);
        }
    }
}
