﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinematicObject : MonoBehaviour
{
    public int clipId;
    public TroopController currTroop;
    public PuzzleCamPos puzzleToActivate;
    bool once = true; 
    public bool actionAfter;
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (currTroop.enemiesClear && once)
        {
            if (other.GetComponent<Gatto>())
            {
                CinematicManager.main.PlayVideo(clipId);
                once = false;
                if (actionAfter)
                {
                    Action();
                }
            }
        }
    }
    public void Action()
    {
        puzzleToActivate.BeReady();
    }
}
